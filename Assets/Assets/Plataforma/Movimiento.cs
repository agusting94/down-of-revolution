﻿using UnityEngine;
using System.Collections;

public class Movimiento : MonoBehaviour {

    public GameObject plataforma_movil;
    public Transform posicion_inicial;
    public Transform posicion_final;
    private Transform posicion_siguiente;
    public float velocidad;

	// Use this for initialization
	void Start () {
        posicion_siguiente = posicion_final;
	}
	
	// Update is called once per frame
	void Update () {
        plataforma_movil.transform.position = Vector2.MoveTowards(plataforma_movil.transform.position, posicion_siguiente.position, Time.deltaTime * velocidad);

        if(plataforma_movil.transform.position == posicion_siguiente.position)
        {
            posicion_siguiente = posicion_siguiente == posicion_final ? posicion_inicial : posicion_final;
        }
	}
}
